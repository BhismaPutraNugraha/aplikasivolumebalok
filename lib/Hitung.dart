import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenghitungVolumeBalok extends StatefulWidget {
  @override
  _MenghitungVolumeBalokState createState() => _MenghitungVolumeBalokState();
}

class _MenghitungVolumeBalokState extends State<MenghitungVolumeBalok> {
  double panjang = 0;
  double lebar = 0;
  double tinggi = 0;
  double volume = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent,
        title: Text(
          "Aplikasi Menghitung Volume Balok",
          style: TextStyle(color: Colors.black),
        ),
        leading: Icon(
          Icons.dashboard_outlined,
          color: Colors.black,
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            height: 250,
            child: Image.asset(
              "assets/images/Balok.jpg",
            ),
          ),
          Text(
            "Mari Kita Menghitung Volume Balok !",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Row(
            children: [
              Text("Panjang Balok"),
              Expanded(
                child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      panjang = double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 6,
                ),
              )
            ],
          ),
          Row(
            children: [
              Text("Lebar Balok"),
              Expanded(
                child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      lebar = double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 6,
                ),
              )
            ],
          ),
          Row(
            children: [
              Text("Tinggi Balok"),
              Expanded(
                child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      tinggi = double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 6,
                ),
              )
            ],
          ),
          // ignore: deprecated_member_use
          RaisedButton(
            onPressed: () {
              setState(() {
                volume = panjang * lebar * tinggi;
              });
            },
            child: Text("Hitung !"),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "Volume dari Balok =$volume",
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.green,
                  fontStyle: FontStyle.italic),
            ),
          ])
        ],
      ),
    );
  }
}
